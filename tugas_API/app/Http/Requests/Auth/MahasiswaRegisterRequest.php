<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class MahasiswaRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'user_id' => ['required'],
            'nama' => ['required'],
            'nim' => ['required', 'unique:mahasiswas,nim'],
            'fakultas' => ['required'],
            'jurusan' => ['required'],
            'no_hp' => ['required'],
            'no_wa' => ['required'],
        ];
    }
}
