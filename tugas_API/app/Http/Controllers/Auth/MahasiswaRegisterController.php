<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\models\Mahasiswa;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\Auth\MahasiswaRegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MahasiswaRegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //yang boleh mengisi data diri adalah yang sudah punya akun
        $this->middleware('auth:api');
    }

    public function __invoke(MahasiswaRegisterRequest $request)
    {
        //dd(Auth::user ()->id);
        Mahasiswa::create([
            'user_id' => Auth::user()->id,
            'nama' => request('nama'),
            'nim' => request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'no_hp' => request('no_hp'),
            'no_wa' => request('no_wa'),
        ]);

        return response('Thanks, you are registered as Mahasiswa.');

    }
}
