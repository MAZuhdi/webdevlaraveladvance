<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Route;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //SuperAdmin Exclussive
        if ($request->route()->named('route1')) {
            if (Auth::user()->isSuperAdmin()) {
                return $next($request);
            }
            abort(401);
        }
        //Admin Exclussive
        if ($request->route()->named('route2')) {
            if (Auth::user()->isSuperAdmin()) {
                return $next($request);
            }
            if (Auth::user()->isAdmin()) {
                return $next($request);
            }
            abort(402);
        }
        //Basic (Guest)
        if ($request->route()->named('route3')) {
            return $next($request);
        }



    }
}
