<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'admin'])->group(function(){
    //SuperAdmin Exclussive
    Route::get('/route-1', 'testController@route1')->name('route1');
    //Admin Exclussive
    Route::get('/route-2', 'testController@route2')->name('route2');
    //Basic (Guest)
    Route::get('/route-3', 'testController@route3')->name('route3');
});
