<?php
//Import Class Elang di file Elang.php 
require_once 'Elang.php';
//Import Harimau di file Harimau.php
require_once 'Harimau.php';
//Import Trait Hewan di file tr_Hewan.php
require_once 'tr_Hewan.php';
//Import Trait Fight di file tr_Fight.php
require_once 'tr_Fight.php';

$Elang1 = new Elang("Phoenix");
$Harimau1 = new Harimau("Leon");

echo $Elang1->atraksi();
echo $Elang1->serang($Harimau1);
echo $Harimau1->getiInfoHewan();

?>