<?php
//Import Trait Hewan dan Fight di file tr_Hewan.php dan tr_Fight.php
require_once 'tr_Hewan.php';
require_once 'tr_Fight.php';

class Harimau {
    use Hewan;
    use Fight;

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahkaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getiInfoHewan()
    {
        return
        "--Stat of {$this->nama}--<br>
         Jenis Hewan : Harimau<br>
         Nama : " . $this->nama."<br>
         Darah : " . $this->darah."<br>
         Jumlah Kaki : " . $this->jumlahkaki."<br>
         Keahlian : " . $this->keahlian."<br>
         Attack Power : " . $this->attackPower."<br>
         Defence Power : " . $this->defencePower."<br>";
    }
}
?>