<?php
/**
 * Trait ini tidak bisa diinstansiasi
 */
trait Fight
{
    //property
    public $attackPower;
    public $defencePower;

    //methods
    public function serang($target)
    {
        $target->diserang($this);
        return "{$this->nama} sedang menyerang {$target->nama}<br>";
    }
    public function diserang($penyerang)
    {
        //mengubah stat 
        $this->darah = $this->darah - $penyerang->attackPower / $this->defencePower;
        return "{$this->nama} sedang diserang<br>";
    }
}
?>