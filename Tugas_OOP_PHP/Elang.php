<?php
//Import Trait Hewan dan Fight di file tr_Hewan.php dan tr_Fight.php
require_once 'tr_Hewan.php';
require_once 'tr_Fight.php';

class Elang {
    use Hewan;
    use Fight;

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahkaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getiInfoHewan()
    {
        return
        "--Stat of {$this->nama}--<br>
         Jenis Hewan : Elang<br>
         Nama : " . $this->nama."<br>
         Darah : " . $this->darah."<br>
         Jumlah Kaki : " . $this->jumlahkaki."<br>
         Keahlian : " . $this->keahlian."<br>
         Attack Power : " . $this->attackPower."<br>
         Defence Power : " . $this->defencePower."<br>";
    }
}

?>