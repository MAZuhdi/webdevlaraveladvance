<?php
/**
 * Trait ini tidak bisa diinstansiasi
 */
trait Hewan {

    //property
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;

    //methods
    public function atraksi()
    {
        return "{$this->nama} sedang {$this->keahlian}<br>";
    }
}
?>