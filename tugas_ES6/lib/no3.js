// 3. Destructuring
// Diberikan sebuah objek sebagai berikut:

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  // dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
  
  // const firstName = newObject.firstName;
  // const lastName = newObject.lastName;
  // const destination = newObject.destination;
  // const occupation = newObject.occupation;
  // Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
  
  //ES6
  let {firstName, lastName, destination, occupation} = newObject
  // Driver code
  console.log(firstName, lastName, destination, occupation)