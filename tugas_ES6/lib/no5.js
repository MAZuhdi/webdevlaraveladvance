// 5. Template Literals
// sederhanakan string berikut agar menjadi 
// lebih sederhana menggunakan template 
// literals ES6:

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + ' dolor sit amet, ' 
+  'consectetur adipiscing elit,' + planet 
+ 'do eiusmod tempor '
+ 'incididunt ut labore et dolore magna aliqua. Ut enim' 
+ ' ad minim veniam'   
// Driver Code 
console.log(before)

let after = `lorem ${view} dolor sit amet, cosectetur adipiscing elit, ${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(after)