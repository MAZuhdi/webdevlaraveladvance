////////////////////////VAR

//var nama = 'Hilman';
//console.log(nama);

////////////////////////LET

// let x = "Agung"; 

// { //->ini block
//     let x = "Zuhdi"

//     console.log(x); //hasilnya Zuhdi
// }

// console.log(x) //hasilnya Agung

////////////////////////CONST
// const person = {
//     id : 1,
//     name : 'Agung'
// };

// console.log(person);

// //masih bisa diubah caranya:
// person.id = 2;
// console.log(person);
// //masih bisa ditambah
// person.age = 20;
// console.log(person);


////////////////////////Function Arrow

// let members = ['Muhammad', 'Agung', 'Zuhdi'];

// members.forEach((member, index) => {
//     console.log(member + ' ' + index )
// })

// let membersindex = members.map((member, index) => /*{*/
//     /*return*/ (member + ' ' + index)
// /*}*/ )

// let membersindex1 = members.map((member, index) => 
//     (member + ' ' + index))

// console.log(membersindex);
// console.log(membersindex1);

///Non Arrow
// let SekolahKoding = {
//     members : ['Muhammad', 'Agung', 'Zuhdi'],
//     getMembers(){
//         this.members.map(function() {
//             console.log(this)  //this mengacu ke window
//         })
//     }
// }
// SekolahKoding.getMembers();

//with Arrow
// let SekolahKoding = {
//     members : ['Muhammad', 'Agung', 'Zuhdi'],
//     getMembers(){
//         this.members.map( name => {
//             console.log(this)  //this mengacu ke objectnya sendiri
//         })
//     }
// }
// SekolahKoding.getMembers();

// const golden = () => {
//     console.log("This is Golden!!")
// }

// golden()

////////////////////////Default Parameter
// generateTitle = () => {
//     return 'ini judul default'
// }

// createTag = (title = generateTitle(), tag='css') => {  //caraES6
//     //carajadul1
//     // tag = tag || 'css'

//     //carajadul2
//     // if (typeof tag === 'undefined') {
//     //     tag = 'css'
//     // }

//     console.log('Judulnya ' + title + ' Tagnya ' + tag )
// }

// createTag();


////////////////////////Rest and Spread
// //1. dipisah saat dipanggil
// signIn = (username, password) => {
//     console.log(username + ' paswotnya ' + password)
// }

// let data = ['MAZuhdi', 'rahasia']

// //cara jadul
// signIn(data[0], data[1]);
// //cara ES6
// signIn(...data);

// //bisa kebalikannya
// //2. digabungin saat diambil

// signInGabung = (...member) => {
//     console.log(member)
// }

// username    = 'MAZuhdi'
// password    = 'rahasia'
// umur        = 20

// signInGabung(username, password, umur)

// 4. Array Spreading
// Kombinasikan dua array berikut menggunakan 
// array spreading ES6

// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = [...west, ...east]
// //Driver Code
// console.log(combined)





////////////////////////Template Literal

// let username = 'Agung'
// let umur = 20
// //jadul
// // let text = 'Member ' + username + ' umurnya ' + 20
// //ES6
// let text = `Member ${username} umurnya ${umur}`
// console.log(text)

//gabisa di js
// let div = "
//     <div> Agung </div>
//     <p> 20</p>
// ";

//ganti pake petik kebalik, malah bisa literal
// let div = `
//     <div> ${username} </div>
//     <p> ${umur} </p>
// `

// console.log(div);

//function

// test = (strings, username) => {
//     let string1 = strings[0]
//     let string2 = strings[1]
    
//     console.log(string1)
//     console.log(string2)
// }

// let output = test`nama saya adalah ${username} umurnya ${umur}`

// 5. Template Literals
// sederhanakan string berikut agar menjadi 
// lebih sederhana menggunakan template 
// literals ES6:

// const planet = "earth" 
// const view = "glass" 
// var before = 'Lorem ' + view + ' dolor sit amet, ' 
// +  'consectetur adipiscing elit,' + planet 
// + 'do eiusmod tempor '
// + 'incididunt ut labore et dolore magna aliqua. Ut enim' 
// + ' ad minim veniam'   
// // Driver Code 
// console.log(before)

// let after = `lorem ${view} dolor sit amet, cosectetur adipiscing elit, ${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// console.log(after)


////////////////////////Short Hand / Penulisan singkat
// nama = 'Yp'
// umur = '20'

// getData = () => {
//     return `member ${nama} umurnya ${umur}`
// }

//jadul
// let member = {
//     nama : nama,
//     umur : umur,
// }
// console.log(member);

// ES6
// let member = {
//     nama, umur, getData
// }

// console.log(member.getData())

// 2. Sederhanakan menjadi Object literal di ES6
// return dalam fungsi di bawah ini masih menggunakan object 
// literal dalam ES5, ubahlah menjadi bentuk yang lebih 
// sederhana di ES6.

// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//       return 
//     }
//   }
// }

const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
  }
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

////////////////////////Destructuring / Mecah objek 

// let member = {
//     name : 'Agung',
//     umur : 20
// }

// let {name, umur : age} = member

// console.log(name) //bisa langsung manggil
// console.log(age); //ngasih nama baru

// //misahin array juga bisa
// let angka = [1, 2, 3]
// let[a,b,c] = angka
// console.log(a+b+c)

// 3. Destructuring
// Diberikan sebuah objek sebagai berikut:

// const newObject = {
//   firstName: "Harry",
//   lastName: "Potter Holt",
//   destination: "Hogwarts React Conf",
//   occupation: "Deve-wizard Avocado",
//   spell: "Vimulus Renderus!!!"
// }
// // dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

// // const firstName = newObject.firstName;
// // const lastName = newObject.lastName;
// // const destination = newObject.destination;
// // const occupation = newObject.occupation;
// // Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

// //ES6

// let {firstName, lastName, destination, occupation} = newObject
// // Driver code
// console.log(firstName, lastName, destination, occupation)

