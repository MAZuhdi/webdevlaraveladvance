// 2. Sederhanakan menjadi Object literal di ES6
// return dalam fungsi di bawah ini masih menggunakan object 
// literal dalam ES5, ubahlah menjadi bentuk yang lebih 
// sederhana di ES6.

// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//       return 
//     }
//   }
// }

const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
  }
 
//Driver Code 
newFunction("William", "Imoh").fullName() 
